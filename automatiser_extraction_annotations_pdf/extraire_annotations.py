
# coding: utf-8


class Annotation:
    
    def _getObjetPdf(nom_fichier_pdf: str)->"objet_pdf_poppler":
        import popplerqt5
        d = popplerqt5.Poppler.Document.load(nom_fichier_pdf)
        return d
    
   
    
    def _get_numero_page_max(ObjetPdf: "objet Pdf Poppler qt5")->int : 
        return ObjetPdf.numPages() - 1
    
    
    
    def _getObjetPageParIndex(index: int, objetPdf: "objet pdf poppler qt5")-> "objet_page":
        objet_page = objetPdf.page(index)
        return objet_page
   
    def _getObjetAnnotation(ObjetPage : "objetPage") -> "objetAnnotation":
        objet_annotation = ObjetPage.annotations()
        return objet_annotation
    
   
    
    
    def _estAnnotationNonVide(ObjetAnnotation : "objet_annotation") -> bool:
        eNv = False
        if len(ObjetAnnotation.contents()) != 0:
            eNv = True
        return eNv
    
    def _estObjetSurlignage(ObjetAnnotation : "objet_annotation") -> bool:
        import popplerqt5
        eosl = False
        if(isinstance(ObjetAnnotation, popplerqt5.Poppler.HighlightAnnotation)):
            eosl = True
        return eosl    
   

    def _getAnnotationSurlignee(ObjetAnnotation : "objet_annotation_surlignee", ObjetPage : "objet_page") -> "str":
        from PyQt5 import QtCore
        pwidth  = Annotation._getLargeurPage(ObjetPage)
        pheight = Annotation._getHauteurPage(ObjetPage)
        chaine_resultante = "" 
        quads = ObjetAnnotation.highlightQuads()
        for quad in quads:
            rect_coords = (quad.points[0].x() * pwidth,
                           quad.points[0].y() * pheight,
                           quad.points[2].x() * pwidth,
                           quad.points[2].y() * pheight)
            rect = QtCore.QRectF()
            rect.setCoords(*rect_coords)
            rect_txt = ObjetPage.text(rect)
            if rect_txt == '':
                chaine_resultante +=  "---\n"
            else:
                chaine_resultante += f"== highlighted text: {rect_txt}\n"
        return chaine_resultante


    def _getAnnotationTexte(ObjetAnnotation : "objet_annotation_texte") -> "str":
        return ObjetAnnotation.contents()
 
    def _getLargeurPage(ObjetPage: "objet_page") -> int :
        pass
        pwidth = ObjetPage.pageSize().width()
        return pwidth

    def _getHauteurPage(ObjetPage: "objet_page") -> int:
        pass 
        pheight = ObjetPage.pageSize().height()
        return pheight

    def yieldAnnotations(str_chemin_fichier : str)-> "str les annotations du pdf ou str_pas_dannoationttoruvee":
    
        d = Annotation._getObjetPdf(str_chemin_fichier)
        #d = _getObjetPdf('python - Parse annotations from a pdf - Stack Overflow.pdf')
        for index_page in range(*list({'start':0, 'stop_exclu':Annotation._get_numero_page_max(d) + 1, 'step':1}.values())):
            print("~~~~~~~~~~~examen de la page numero: ", str(index_page))    
            la_page = Annotation._getObjetPageParIndex(index_page,d)
    
    
            for annot in Annotation._getObjetAnnotation(la_page):
                if Annotation._estAnnotationNonVide(annot):
                    #print("nouvellle annotation")
                    yield Annotation._getAnnotationTexte(annot) 

                if Annotation._estObjetSurlignage(annot):
                    yield Annotation._getAnnotationSurlignee(annot, la_page)
                    #print("contenu: ", annot.contents())
                    #print("lg contenu:", str(len(annot.contents())))
                    #print("methodes: " ,dir(annot))
                    #print("type:" , type(annot))
                    #print("soustype: ", annot.SubType()) 
                    #print("----------")
            #print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~fin examen page")



def main():
    import sys
    nom_fichier = sys.argv[1]
    for chaine in Annotation.yieldAnnotations(nom_fichier):
        print(chaine)

if __name__ == "__main__":
    
    main()
